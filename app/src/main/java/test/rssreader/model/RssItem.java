package test.rssreader.model;


import android.content.ContentValues;
import android.os.Parcel;
import android.os.Parcelable;

import test.rssreader.database.RssContentTable;

public class RssItem implements Parcelable {

    private String title;
    private String menuTitle;
    private String date;
    private String description;
    private String link;
    private String id;
    private String mThumbnail;

    protected RssItem(Parcel in) {
        title = in.readString();
        menuTitle = in.readString();
        date = in.readString();
        description = in.readString();
        link = in.readString();
        id = in.readString();
        mThumbnail = in.readString();
    }


    public static final Creator<RssItem> CREATOR = new Creator<RssItem>() {
        @Override
        public RssItem createFromParcel(Parcel in) {
            return new RssItem(in);
        }

        @Override
        public RssItem[] newArray(int size) {
            return new RssItem[size];
        }
    };

    public String getMenuTitle() {
        return menuTitle;
    }

    public void setMenuTitle(String menuTitle) {
        this.menuTitle = menuTitle;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getmThumbnail() {
        return mThumbnail;
    }

    public void setmThumbnail(String mThumbnail) {
        this.mThumbnail = mThumbnail;
    }

    public RssItem(){}

    public RssItem(String menuTitle, String id, String title, String date, String description, String link, String mThumbnail){
        this.menuTitle = menuTitle;
        this.id = id;
        this.title = title;
        this.date = date;
        this.description = description;
        this.link = link;
        this.mThumbnail = mThumbnail;
    }



    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ContentValues toValues() {
        ContentValues values = new ContentValues();
        values.put(RssContentTable.COLUMN_MENU_TITLE, menuTitle);
        values.put(RssContentTable.COLUMN_ID, id);
        values.put(RssContentTable.COLUMN_TITLE, title);
        values.put(RssContentTable.COLUMN_LINK, link);
        values.put(RssContentTable.COLUMN_PUB_DATE, date);
        values.put(RssContentTable.COLUMN_THUMBNAIL, mThumbnail);
        values.put(RssContentTable.COLUMN_DESCRIPTION, description);
        return values;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(menuTitle);
        parcel.writeString(date);
        parcel.writeString(description);
        parcel.writeString(link);
        parcel.writeString(id);
        parcel.writeString(mThumbnail);
    }
}
