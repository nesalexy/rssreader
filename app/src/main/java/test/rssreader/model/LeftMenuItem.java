package test.rssreader.model;


import android.content.ContentValues;

import test.rssreader.database.RssItemTable;

public class LeftMenuItem {

    String name_item;
    String link_item;
    String id_item;

    public LeftMenuItem() {}

    public LeftMenuItem(String name_item, String link_item, String id_item) {
        this.name_item = name_item;
        this.link_item = link_item;
        this.id_item = id_item;
    }

    public ContentValues toValues() {
        ContentValues values = new ContentValues();
        values.put(RssItemTable.COLUMN_ID, id_item);
        values.put(RssItemTable.COLUMN_LINK, link_item);
        values.put(RssItemTable.COLUMN_TITLE, name_item);
        return values;
    }

    public String getId_item() {
        return id_item;
    }

    public void setId_item(String id_item) {
        this.id_item = id_item;
    }

    public String getLink_item() {
        return link_item;
    }

    public void setLink_item(String link_item) {
        this.link_item = link_item;
    }

    public LeftMenuItem(String name_item){
        this.name_item = name_item;
    }

    public String getName_item() {
        return name_item;
    }

    public void setName_item(String name_item) {
        this.name_item = name_item;
    }
}
