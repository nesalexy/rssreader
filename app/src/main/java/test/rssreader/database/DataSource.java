package test.rssreader.database;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import test.rssreader.model.LeftMenuItem;
import test.rssreader.model.RssItem;

public class DataSource {

    private Context mContext;
    private SQLiteDatabase mDatabase;
    private SQLiteOpenHelper mDBHelper;
    private Cursor mCursor;

    public DataSource(Context context) {
        mContext = context;
        mDBHelper = new DBHelper(mContext);
        mDatabase = mDBHelper.getWritableDatabase();
    }

    public void open() {
        mDatabase = mDBHelper.getWritableDatabase();
    }

    public void close() {
        mDBHelper.close();
    }

    public void createRssContent(RssItem rssItem) {
        ContentValues contentValues = rssItem.toValues();
        mDatabase.insert(RssContentTable.RSS_TABLE, null, contentValues);
    }

    public void createRssItem(LeftMenuItem leftMenuItem) {
        ContentValues contentValues = leftMenuItem.toValues();
        mDatabase.insert(RssItemTable.RSS_ITEM_TABLE, null, contentValues);
    }


    //standart sort
    public ArrayList<RssItem> getRssContentItemById(String id){
        String selection = "itemId = '" + id + "'";
        ArrayList<RssItem> rssItems = new ArrayList<>();
        mCursor = mDatabase.query(RssContentTable.RSS_TABLE, RssContentTable.ALL_COLUMNS, selection, null, null, null, RssContentTable.COLUMN_PUB_DATE + " DESC");
        rssItems.addAll(getRssItemsList(mCursor));
        return rssItems;
    }

    public ArrayList<RssItem> getRssContentItemByIdASC(String id){
        String selection = "itemId = '" + id + "'";
        ArrayList<RssItem> rssItems = new ArrayList<>();
        mCursor = mDatabase.query(RssContentTable.RSS_TABLE, RssContentTable.ALL_COLUMNS, selection, null, null, null, RssContentTable.COLUMN_PUB_DATE + " ASC");
        rssItems.addAll(getRssItemsList(mCursor));
        return rssItems;
    }

    public ArrayList<RssItem> getRssItemsList(Cursor mCursor){
        ArrayList<RssItem> rssItems = new ArrayList<>();

        while (mCursor.moveToNext()) {
            RssItem rssItem = new RssItem();
            rssItem.setId(mCursor.getString(mCursor.getColumnIndex(RssContentTable.COLUMN_ID)));
            rssItem.setDescription(mCursor.getString(mCursor.getColumnIndex(RssContentTable.COLUMN_DESCRIPTION)));
            rssItem.setDate(mCursor.getString(mCursor.getColumnIndex(RssContentTable.COLUMN_PUB_DATE)));
            rssItem.setTitle(mCursor.getString(mCursor.getColumnIndex(RssContentTable.COLUMN_TITLE)));
            rssItem.setLink(mCursor.getString(mCursor.getColumnIndex(RssContentTable.COLUMN_LINK)));
            rssItem.setmThumbnail(mCursor.getString(mCursor.getColumnIndex(RssContentTable.COLUMN_THUMBNAIL)));

            rssItems.add(rssItem);
        }

        return rssItems;
    }

    public ArrayList<LeftMenuItem> getAllItemMenu(){
        ArrayList<LeftMenuItem> leftMenuItems = new ArrayList<>();
        mCursor = mDatabase.query(true, RssItemTable.RSS_ITEM_TABLE, null, null, null, RssItemTable.COLUMN_ID, null, null, null);
        while (mCursor.moveToNext()) {
            LeftMenuItem leftMenuItem = new LeftMenuItem();
            leftMenuItem.setName_item(mCursor.getString(mCursor.getColumnIndex(RssItemTable.COLUMN_TITLE)));
            leftMenuItem.setId_item(mCursor.getString(mCursor.getColumnIndex(RssItemTable.COLUMN_ID)));
            leftMenuItem.setLink_item(mCursor.getString(mCursor.getColumnIndex(RssItemTable.COLUMN_LINK)));

            leftMenuItems.add(leftMenuItem);
        }

        return leftMenuItems;
    }



    public void deleteItemFromDb(String id, String table){
        mDatabase.delete(table, RssContentTable.COLUMN_ID + " = ?", new String[]{id});
    }

}
