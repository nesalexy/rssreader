package test.rssreader.database;

public class RssItemTable {

    public static final String COLUMN_ID = "itemId";
    public static final String RSS_ITEM_TABLE = "rss_item";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_LINK = "link";



    public static final String CREATE_RSS_ITEM_TABLE =
            "CREATE TABLE " + RSS_ITEM_TABLE + "(" +
                    COLUMN_ID + " TEXT, " +
                    COLUMN_TITLE + " TEXT, " +
                    COLUMN_LINK + " TEXT " + ");";


    public static final String DELETE_RSS_TABLE =
            "DROP TABLE " + RSS_ITEM_TABLE;
}
