package test.rssreader.database;


public class RssContentTable {

    public static final String COLUMN_ID = "itemId";
    public static final String RSS_TABLE = "rss";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_MENU_TITLE = "menuTitle";
    public static final String COLUMN_LINK = "link";
    public static final String COLUMN_PUB_DATE = "pubDate";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_THUMBNAIL = "thumbnail";

    public static final String[] ALL_COLUMNS = {COLUMN_ID, COLUMN_TITLE, COLUMN_LINK,
            COLUMN_PUB_DATE, COLUMN_DESCRIPTION, COLUMN_THUMBNAIL};

    public static final String[] LEFT_MENU_COLUMNS = {COLUMN_ID, COLUMN_MENU_TITLE, COLUMN_LINK};

    public static final String[] ID_COLUMNS = { COLUMN_ID };


    public static final String CREATE_RSS_TABLE =
            "CREATE TABLE " + RSS_TABLE + "(" +
                    COLUMN_ID + " TEXT, " +
                    COLUMN_TITLE + " TEXT, " +
                    COLUMN_LINK + " TEXT, " +
                    COLUMN_MENU_TITLE + " TEXT, " +
                    COLUMN_THUMBNAIL + " TEXT, " +
                    COLUMN_PUB_DATE + " TEXT, " +
                    COLUMN_DESCRIPTION + " TEXT " + ");";

    public static final String DELETE_RSS_TABLE =
            "DROP TABLE " + RSS_TABLE;
}
