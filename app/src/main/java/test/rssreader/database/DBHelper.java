package test.rssreader.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

    private static final String DB_FILE_NAME = "news.db";
    public static final int DB_VERSION = 2;

    public DBHelper(Context context) {
        super(context, DB_FILE_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(RssContentTable.CREATE_RSS_TABLE);
        sqLiteDatabase.execSQL(RssItemTable.CREATE_RSS_ITEM_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(RssContentTable.DELETE_RSS_TABLE);
        sqLiteDatabase.execSQL(RssItemTable.DELETE_RSS_TABLE);

    }
}
