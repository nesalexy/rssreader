package test.rssreader.util;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


public class ISO8601 {
    //Wed, 18 Jan 2017 01:12:59 +0200
    public static SimpleDateFormat sDf = new SimpleDateFormat("dd MMMM yyyy", Locale.ENGLISH);
    static SimpleDateFormat dateformat = new SimpleDateFormat("EEE, dd MMM yyyy", Locale.getDefault());

    long timestamp = 0;

    public String generationNormalDate(String isoDate){
        String dateToString = "";
        dateformat.setTimeZone(TimeZone.getTimeZone("UTC"));

        if(isoDate.isEmpty()){
            return "";
        }

        try {
            Date date = dateformat.parse(isoDate);
            timestamp = date.getTime();

            dateToString = sDf.format(timestamp);

        } catch (ParseException e) {
            Log.e("ParseException", e.toString());
            e.printStackTrace();
        }

        return dateToString;
    }


}
