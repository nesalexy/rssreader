package test.rssreader.view.activitys.mainActivity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import test.rssreader.R;
import test.rssreader.database.DataSource;
import test.rssreader.database.RssContentTable;
import test.rssreader.database.RssItemTable;
import test.rssreader.model.LeftMenuItem;
import test.rssreader.model.RssItem;
import test.rssreader.util.NetworkUntils;
import test.rssreader.view.adapters.LeftMenuAdapter;
import test.rssreader.view.fragments.FragmentParent;

public class MainActivity extends AppCompatActivity  {

    private RecyclerView menu;
    private FragmentParent fragmentParent;

    private RecyclerView.LayoutManager mLayoutManager;
    private LeftMenuAdapter menuAdapter;
    private Button refreshCurrentRss;
    private RelativeLayout bg_popup;
    private TextView add_rss;

    private ArrayList<LeftMenuItem> leftMenuItems;
    private PopupWindow popupWindow;
    private DrawerLayout drawer;

    private DataSource mDataSource;
    private int currentTabPos = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mDataSource = new DataSource(this);
        mDataSource.open();

        leftMenuItems = new ArrayList<>();
        leftMenuItems.add(new LeftMenuItem(getString(R.string.add_rss_feed)));

        fragmentParent = (FragmentParent) this.getSupportFragmentManager().findFragmentById(R.id.fragmentParent);
        menu = (RecyclerView) findViewById(R.id.menu_item);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        bg_popup = (RelativeLayout) findViewById(R.id.bg_popup);
        add_rss = (TextView) findViewById(R.id.add_rss);
        refreshCurrentRss = (Button) findViewById(R.id.refreshCurrentRss);

        mLayoutManager = new LinearLayoutManager(this);
        menu.setLayoutManager(mLayoutManager);
        menuAdapter = new LeftMenuAdapter(leftMenuItems, MainActivity.this);
        menu.setAdapter(menuAdapter);

        //action listener add, delete
        menuAdapter.setOnItemClickListenerDelete(new LeftMenuAdapter.ActionOnItemClickListener() {
            @Override
            public void onItemClick(LeftMenuItem place, int position) {
                if(position == leftMenuItems.size()-1) showPopUp();
                else {

                    //delete item
                    try {
                        mDataSource.deleteItemFromDb(place.getId_item(), RssContentTable.RSS_TABLE);
                        mDataSource.deleteItemFromDb(place.getId_item(), RssItemTable.RSS_ITEM_TABLE);

                        Log.e("remove", String.valueOf(position));

                        fragmentParent.removeFragment(position);
                        leftMenuItems.remove(position);
                        menuAdapter.notifyDataSetChanged();

                        if(fragmentParent.getSizeChildFragments() <= 0) {
                            refreshCurrentRss.setVisibility(View.INVISIBLE);
                            showRssText();
                            fragmentParent.hideFragmentParent();
                        }

                        if (drawer.isDrawerOpen(GravityCompat.START)) {
                            drawer.closeDrawer(GravityCompat.START);
                        }

                    }catch (Exception e) {
                        Log.e("error", e.toString());
                    }
                }
            }
        });

        //choose listener
        menuAdapter.setOnItemClickListenerChoose(new LeftMenuAdapter.ChooseItemListener() {
            @Override
            public void onItemClick(LeftMenuItem place, int position) {
                if(position != leftMenuItems.size()-1) {
                    fragmentParent.setItem(position);
                    if(popupWindow != null) popupWindow.dismiss();
                    if (drawer.isDrawerOpen(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    }
                }
            }
        });

        //refresh current url
        refreshCurrentRss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LeftMenuItem menuItem = leftMenuItems.get(getCurrentTabPos());
                mDataSource.deleteItemFromDb(menuItem.getId_item(), RssContentTable.RSS_TABLE);

                fragmentParent.parsingXml(menuItem.getName_item(), menuItem.getLink_item(), true, menuItem.getId_item());
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();



        //get item menu from data base if exist
        getItemLeftMenu();
    }

    public void getItemLeftMenu(){
        ArrayList<LeftMenuItem> leftMenuItems = mDataSource.getAllItemMenu();

        if(leftMenuItems.size() > 0) {
            for(LeftMenuItem leftMenuItem : leftMenuItems) {
                addItemToLeftMenu(leftMenuItem);
                fragmentParent.addPageFromDb(leftMenuItem.getName_item(), leftMenuItem.getId_item());
            }


        } else {
            //db is empty
            refreshCurrentRss.setVisibility(View.INVISIBLE);
            showRssText();
            fragmentParent.hideFragmentParent();
        }
    }

    public void showRssText(){
        add_rss.setVisibility(View.VISIBLE);
    }

    public void hideRssText(){
        add_rss.setVisibility(View.GONE);
    }

    public void showPopUp(){
        bg_popup.setVisibility(View.VISIBLE);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }

        LayoutInflater inflater = (LayoutInflater) MainActivity.this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.popup_window,
                (ViewGroup) findViewById(R.id.popup_element));
        popupWindow = new PopupWindow(layout, 400, 300, true);
        popupWindow.getContentView().setBackgroundResource(android.R.color.transparent);
        popupWindow.showAtLocation(layout, Gravity.CENTER, 0, 0);
        popupWindow.setFocusable(true);
        popupWindow.setTouchable(true);
        popupWindow.setOutsideTouchable(true);

        Button btn_add = (Button) layout.findViewById(R.id.btn_add);
        Button btn_cancel = (Button) layout.findViewById(R.id.btn_cancel);

        final EditText name = (EditText) layout.findViewById(R.id.name_item);
        final EditText link = (EditText) layout.findViewById(R.id.link_item);

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String nameItem = name.getText().toString();
                String linkItem = link.getText().toString();

                if(NetworkUntils.isConnect(MainActivity.this)){
                    if(!nameItem.isEmpty() && !linkItem.isEmpty()) {
                        if(URLUtil.isValidUrl(linkItem)) {

                            bg_popup.setVisibility(View.GONE);
                            popupWindow.dismiss();
                            fragmentParent.parsingXml(nameItem, linkItem, false, null);

                        } else {
                            Toast.makeText(getApplicationContext(), getString(R.string.not_valid_link), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.please_add_title_link), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.network_connection_problem), Toast.LENGTH_SHORT).show();
                }

            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
                bg_popup.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onBackPressed() {

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mDataSource.close();
    }

    public void addItemToLeftMenu(LeftMenuItem leftMenuItem){
        leftMenuItems.add(leftMenuItems.size()-1, leftMenuItem);
        menuAdapter.notifyDataSetChanged();
    }

    public void addRssContent(RssItem rssItem){
        mDataSource.createRssContent(rssItem);
    }

    public void addRssItem(LeftMenuItem leftMenuItem){
        mDataSource.createRssItem(leftMenuItem);
        addItemToLeftMenu(leftMenuItem);
    }

    public ArrayList<RssItem> getRssItemsById(String id){
        return mDataSource.getRssContentItemById(id);
    }

    public ArrayList<RssItem> getRssItemsByIdASC(String id){
        return mDataSource.getRssContentItemByIdASC(id);
    }

    public void checkLeftMenuSize(){
        if(leftMenuItems.size() > 0){
            refreshCurrentRss.setVisibility(View.VISIBLE);
        } else {
            refreshCurrentRss.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mDataSource.open();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_order, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(leftMenuItems.size() > 1) {
            LeftMenuItem menuItem = leftMenuItems.get(getCurrentTabPos());

            switch (item.getItemId()) {
                case R.id.order_des:
                    fragmentParent.refreshCurrentFragment(getCurrentTabPos(), menuItem.getId_item());
                    return true;
                case R.id.order_asc:
                    fragmentParent.refreshCurrentFragmentACS(getCurrentTabPos(), menuItem.getId_item());
                    return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    public void setCurrentTabPos(int currentTabPos) {
        this.currentTabPos = currentTabPos;
    }

    public int getCurrentTabPos(){
        return currentTabPos;
    }
}
