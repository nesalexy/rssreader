package test.rssreader.view.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

import test.rssreader.R;
import test.rssreader.model.RssItem;
import test.rssreader.view.adapters.RssItemsAdapter;


public class FragmentChild extends Fragment {

    private RecyclerView list_rss_item;
    private RecyclerView.LayoutManager mLayoutManager;
    private RssItemsAdapter rssItemsAdapter;

    private ArrayList<RssItem> rssItems;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_child, container, false);

        Bundle bundle = getArguments();
        rssItems = bundle.getParcelableArrayList("list");

        list_rss_item = (RecyclerView) view.findViewById(R.id.list_rss_item);

        mLayoutManager = new LinearLayoutManager(getActivity());
        list_rss_item.setLayoutManager(mLayoutManager);
        rssItemsAdapter = new RssItemsAdapter(rssItems, getActivity());
        list_rss_item.setAdapter(rssItemsAdapter);

        rssItemsAdapter.setOnItemClickListener(new RssItemsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(RssItem rssItem) {
                try {
                    String url = rssItem.getLink();
                    if (!url.startsWith("http://") && !url.startsWith("https://"))
                        url = "http://" + url;
                    else {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(browserIntent);
                    }

                }catch (Exception e) {
                    Toast.makeText(getActivity(), getString(R.string.error_open_link_in_brouser), Toast.LENGTH_SHORT).show();
                    Log.e("open link", e.toString());
                }

            }
        });

        return view;
    }

    public void refreshList(ArrayList<RssItem> rssItems) {
        this.rssItems.clear();
        this.rssItems.addAll(rssItems);
        rssItemsAdapter.notifyDataSetChanged();
    }

}
