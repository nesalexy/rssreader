package test.rssreader.view.fragments;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.UUID;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import test.rssreader.R;
import test.rssreader.model.LeftMenuItem;
import test.rssreader.model.RssItem;
import test.rssreader.view.activitys.mainActivity.MainActivity;
import test.rssreader.view.adapters.ViewPagerAdapter;


public class FragmentParent extends Fragment {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter adapter;

    private String menuTitle;
    private ProgressDialog progressDialog;

    private LinearLayout container_fragment_parent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_parent, container, false);

        container_fragment_parent = (LinearLayout) view.findViewById(R.id.container_fragment_parent);
        viewPager = (ViewPager) view.findViewById(R.id.my_viewpager);
        tabLayout = (TabLayout) view.findViewById(R.id.my_tab_layout);
        adapter = new ViewPagerAdapter(getFragmentManager(), getActivity(), viewPager, tabLayout);
        viewPager.setAdapter(adapter);

        tabLayout.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager) {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                super.onTabSelected(tab);
                viewPager.setCurrentItem(tab.getPosition());
                ((MainActivity) getActivity()).setCurrentTabPos(tab.getPosition());
                Log.e("Selected", "Selected " + tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                super.onTabUnselected(tab);
                Log.e("Unselected", "Unselected " + tab.getPosition());
            }
        });

        return view;
    }

    public void hideFragmentParent(){
        container_fragment_parent.setVisibility(View.GONE);
    }

    public void showFragmentParent(){
        container_fragment_parent.setVisibility(View.VISIBLE);
    }

    public void setItem(int position){
        viewPager.setCurrentItem(position);
    }

    public void removeFragment(int position){
        adapter.removeFrag(position);
    }

    public int getSizeChildFragments() {
        return adapter.getCount();
    }

    //get data from internet
    public void parsingXml(String namePage, String link, boolean isUpdate, String updateUid){
        menuTitle = namePage;
        new GetRss().execute(namePage, link, String.valueOf(isUpdate), updateUid);
    }

    public void refreshCurrentFragment(int position, String id){
        ArrayList<RssItem> rssItems = ((MainActivity) getActivity()).getRssItemsById(id);
        adapter.refreshCurrentFragment(position, rssItems);
    }

    public void refreshCurrentFragmentACS(int position, String id){
        ArrayList<RssItem> rssItems = ((MainActivity) getActivity()).getRssItemsByIdASC(id);
        adapter.refreshCurrentFragment(position, rssItems);
    }

    //get data from data base
    public void addPageFromDb(String namePage, String id) {
        generationRssFragment(id, namePage);
    }

    public ArrayList<RssItem> getRssItemsById(String id) {
        return ((MainActivity) getActivity()).getRssItemsById(id);
    }

    //generation fragment
    public void generationRssFragment(String id, String namePage){
        ArrayList<RssItem> rssItems = ((MainActivity) getActivity()).getRssItemsById(id);

        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("list", rssItems);

        FragmentChild fragmentChild = new FragmentChild();
        fragmentChild.setArguments(bundle);

        adapter.addFrag(fragmentChild, namePage);
        adapter.notifyDataSetChanged();

        if (adapter.getCount() > 0) tabLayout.setupWithViewPager(viewPager);

        viewPager.setCurrentItem(adapter.getCount() - 1);
    }

    class GetRss extends AsyncTask<String, String, Boolean> {
        String uuid = UUID.randomUUID().toString();
        String title = "";
        String link = "";
        boolean isUpdate = false;
        String updateUid = "";

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity(),
                    ProgressDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage(getString(R.string.progress_loading));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(String... strings) {

            title = strings[0];
            link = strings[1];
            isUpdate = Boolean.parseBoolean(strings[2]);
            updateUid = strings[3];

            try{
                URL url = new URL(link);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();

                if(conn.getResponseCode() == HttpURLConnection.HTTP_OK) {

                    InputStream is = conn.getInputStream();

                    //xml parsing
                    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                    DocumentBuilder db = dbf.newDocumentBuilder();

                    //element
                    Document document = db.parse(is);
                    Element element = document.getDocumentElement();

                    //take nodes
                    NodeList nodeList = element.getElementsByTagName("item");

                    if(nodeList.getLength() > 0) {
                        //add item to menu
                        if(!isUpdate) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    ((MainActivity) getActivity()).addRssItem(new LeftMenuItem(title, link, uuid));
                                }
                            });
                        }

                        for(int i = 0; i < nodeList.getLength(); i++) {
                            Element entry = (Element) nodeList.item(i);

                            Element titleE = (Element) entry.getElementsByTagName("title").item(0);
                            Element dateE = (Element) entry.getElementsByTagName("pubDate").item(0);
                            Element descriptionE = (Element) entry.getElementsByTagName("description").item(0);
                            Element linkE = (Element) entry.getElementsByTagName("link").item(0);

                            String _url_thumbnail = "";
                            NodeList media = entry.getElementsByTagName("media:thumbnail");
                            Element thumbnailE = (Element) media.item(0);
                            if(thumbnailE != null) {
                                _url_thumbnail = thumbnailE.getAttribute("url");
                            }

                            String _title = titleE.getFirstChild().getNodeValue();
                            String _description = descriptionE.getFirstChild().getNodeValue();
                            String _date = dateE.getFirstChild().getNodeValue();
                            String _link = linkE.getFirstChild().getNodeValue();

                            //add item content to rss list
                            if(!isUpdate) ((MainActivity) getActivity()).addRssContent(new RssItem(menuTitle, uuid, _title, _date, _description, _link, _url_thumbnail));
                            else ((MainActivity) getActivity()).addRssContent(new RssItem(menuTitle, updateUid, _title, _date, _description, _link, _url_thumbnail));
                        }

                        return true;
                    } else {
                        return false;
                    }

                }

            } catch (Exception e) {
                Log.e("getRss", e.toString());

                final String error = e.toString();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
                    }
                });

                return false;
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            if(result){
                showFragmentParent();
                ((MainActivity) getActivity()).hideRssText();

                if(!isUpdate) {
                    generationRssFragment(uuid, menuTitle);

                } else {
                    refreshCurrentFragment(((MainActivity) getActivity()).getCurrentTabPos(), updateUid);
                }

                ((MainActivity) getActivity()).checkLeftMenuSize();

            } else {
                Toast.makeText(getActivity(), getString(R.string.something_wrong), Toast.LENGTH_SHORT).show();
            }

            if(progressDialog != null)
                progressDialog.dismiss();
        }

    }



}
