package test.rssreader.view.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;
import java.util.ArrayList;

import test.rssreader.R;
import test.rssreader.model.RssItem;
import test.rssreader.util.ISO8601;
import test.rssreader.util.NetworkUntils;


public class RssItemsAdapter extends RecyclerView.Adapter<RssItemsAdapter.ViewHolder> {

    ArrayList<RssItem> rssItems;
    Context context;

    public RssItemsAdapter(ArrayList<RssItem> rssItems, Context context) {
        this.rssItems = rssItems;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rss_item, parent, false);
        ViewHolder vh = new ViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        RssItem rss = rssItems.get(position);
        holder.rssItem = rss;

        holder.title.setText(rss.getTitle());
        holder.description.setText(rss.getDescription());
        holder.date.setText(new ISO8601().generationNormalDate(rss.getDate()));

        if(!rss.getmThumbnail().isEmpty()) {
            Log.e("image", rss.getmThumbnail());
            if(NetworkUntils.isConnect(context))
                new DownloadImageTask(holder.rss_image).execute(rss.getmThumbnail());
        } else {
            holder.rss_image.setBackgroundResource(R.drawable.rss);
        }

    }


    @Override
    public int getItemCount() {
        return rssItems.size();
    }

    private static OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(RssItem rssItem);
    }
    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.mOnItemClickListener = onItemClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public RssItem rssItem;
        TextView title, date, description;
        ImageView rss_image;

        public ViewHolder(View v) {
            super(v);

            title = (TextView) v.findViewById(R.id.rss_title);
            date = (TextView) v.findViewById(R.id.rss_date);
            description = (TextView) v.findViewById(R.id.rss_description);
            rss_image = (ImageView) v.findViewById(R.id.rss_image);

            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(mOnItemClickListener != null && rssItem != null ){
                mOnItemClickListener.onItemClick(rssItem);
            }
        }
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);

            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }

            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            if(result != null)
                bmImage.setImageBitmap(result);
            else
                bmImage.setBackgroundResource(R.drawable.rss);
        }
    }
}
