package test.rssreader.view.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import test.rssreader.R;
import test.rssreader.model.LeftMenuItem;


public class LeftMenuAdapter extends RecyclerView.Adapter<LeftMenuAdapter.ViewHolder> {

    ArrayList<LeftMenuItem> leftMenuItem;
    Context context;
    private static ActionOnItemClickListener deleteClickListener;
    private static ChooseItemListener chooseItemListener;

    public LeftMenuAdapter(ArrayList<LeftMenuItem> leftMenuItem, Context context) {
        this.leftMenuItem = leftMenuItem;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.menu_item, parent, false);
        ViewHolder vh = new ViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LeftMenuItem m = leftMenuItem.get(position);
        holder.leftMenuItem = m;
        holder.position = position;

        holder.name_item.setText(m.getName_item());

        if(position != getItemCount()-1) holder.action_item.setText("-");
        else holder.action_item.setText("+");
    }

    @Override
    public int getItemCount() {
        return leftMenuItem.size();
    }

    //action delete, add
    public interface ActionOnItemClickListener {
        void onItemClick(LeftMenuItem place, int position);
    }

    //choose
    public interface ChooseItemListener{
        void onItemClick(LeftMenuItem place, int position);
    }

    public void setOnItemClickListenerDelete(ActionOnItemClickListener onItemClickListener){
        this.deleteClickListener = onItemClickListener;
    }

    public void setOnItemClickListenerChoose(ChooseItemListener onItemClickListenerChoose){
        this.chooseItemListener = onItemClickListenerChoose;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        LeftMenuItem leftMenuItem;
        int position;
        TextView name_item;
        TextView action_item;

        public ViewHolder(View v) {
            super(v);

            name_item = (TextView) v.findViewById(R.id.name_item);
            action_item = (TextView) v.findViewById(R.id.add_item);

            action_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(deleteClickListener != null && leftMenuItem != null){
                        deleteClickListener.onItemClick(leftMenuItem, position);
                    }
                }
            });

            name_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(chooseItemListener != null && leftMenuItem != null){
                        chooseItemListener.onItemClick(leftMenuItem, position);
                    }
                }
            });
        }
    }
}
